/***************************************************************************************
*		              Core51模块库说明									
*模块库均用英文命名，防止添加头文件时因中文路径出现BUG，因此写此Readme
***************************************************************************************/
序列  文件夹名				文件名					中文释义
1：Lattice_Screen			latscr					点阵屏						<----------未实现的模块，待修订
2：Matrix_Buttons			mkey						矩阵按键
3：Nixie_Tube				smg						数码管
4：AT24C02					AT24CXX					AT24C02
5：Bus						I2C						IIC							<----------该文件在架构V2.0后删除，并添加进了内核
6：DS1302					ds1302					实时时钟
7：LCD1602					lcd						LCD1602
8：Temperature				temp						DS18B20温度传感器
9：XPT2046					XPT2046					ADC触摸芯片
10：OLED12864				OLED12864				oled12864，0.96寸OLED  IIC接口	||	  oled12864，0.96寸OLED  SPI接口
11：RC522					RC522						RC522读写卡
12：HC_SR04					hc_sr04					超声波
13：MAX7219					MAX7219					MAX7219
14：Fingerprint			Fingerprint				指纹模块，程序兼容YX_F112、AS608指纹模块






