#include "core51_exti.h"

#if STC89C52RC

#if Ex_Interrupt0
/*******************************************************************************
* 函 数 名         : Ex_Interrupt0_Init
* 输    入         : 结构体变量地址
* 输    出         :	空
* 函数功能		    : 外部中断0配置函数
* 详细描述			 :	该函数是初始化外部中断0的函数
*******************************************************************************/
void Ex_Interrupt0_Init(_Parameter_Set *Par_Set)
{
	IT0 = Par_Set->Ex_Int0_Parameter.IT0_Set;							//跳变沿触发方式
	EX0 = Par_Set->Ex_Int0_Parameter.EX0_Set;							//打开INT0的中断允许。	
	EA  = Par_Set->EA_Set;													//打开总中断	
}

/*******************************************************************************
* 函 数 名         : Test_ExIT0
* 输    入         : 空
* 输    出         :	空
* 函数功能		    : 外部中断测试程序
* 详细描述			 :	该函数是放在外部中断0中断服务函数中使用
*******************************************************************************/
void Test_ExIT0(void)
{
	Delay_Xms(20);	 //延时消抖
	
	if(Test_ExIT0_k == 0)
		Test_ExIT0_led = ~Test_ExIT0_led;
}
#endif

#if Ex_Interrupt1
/*******************************************************************************
* 函 数 名         : Ex_Interrupt1_Init
* 输    入         : 结构体变量地址
* 输    出         :	空
* 函数功能		    : 外部中断1配置函数
* 详细描述			 :	该函数是初始化外部中断1的函数
*******************************************************************************/
void Ex_Interrupt1_Init(_Parameter_Set *Par_Set)
{
	IT1 = Par_Set->Ex_Int1_Parameter.IT1_Set;							//跳变沿触发方式
	EX1 = Par_Set->Ex_Int1_Parameter.EX1_Set;							//打开INT1的中断允许。	
	EA  = Par_Set->EA_Set;													//打开总中断	
}

/*******************************************************************************
* 函 数 名         : Test_ExIT1
* 输    入         : 空
* 输    出         :	空
* 函数功能		    : 外部中断测试程序
* 详细描述			 :	该函数是放在外部中断1中断服务函数中使用
*******************************************************************************/
void Test_ExIT1(void)
{
	Delay_Xms(20);	 //延时消抖
	
	if(Test_ExIT1_k == 0)
		Test_ExIT1_led = ~Test_ExIT1_led;
}
#endif

#endif
